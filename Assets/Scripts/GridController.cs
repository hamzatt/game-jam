﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridController : MonoBehaviour {

    public GameObject gridElementPrefab;

    int numberOfElementsInOneRow = 4;
    int startingNumberOfRows = 4;

    int currentRowCount = 0;

    float xStart = -4.5f;
    float xGap = 3.5f;
    float zStart = 0;
    float zGap = 3.5f;

    //Particle Systems
    public GameObject fireParticleSystemPrefab;
    public GameObject waterParticleSystemPrefab;
    public GameObject woodParticleSystemPrefab;
    public GameObject sandParticleSystemPrefab;
    public GameObject mirrorParticleSystemPrefab;
    public GameObject thunderParticleSystemPrefab;
    public GameObject iceParticleSystemPrefab;
    public GameObject windParticleSystemPrefab;
    public GameObject rocksParticleSystemPrefab;

    private int noOfTypes = 9;

    List<int> previousRowType = new List<int>();

    // Use this for initialization
    void Start () {

        for (int i = 0; i < startingNumberOfRows; i++)
        {
            GenerateRow(currentRowCount);
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void GenerateRow(int rowNumber) {
        List<int> currRowType = new List<int>();
        for (int j = 0; j < numberOfElementsInOneRow; j++)
        {
            GameObject instantiatedObject = GameObject.Instantiate(gridElementPrefab);

            Vector3 position = Vector3.zero;
            position.x = xStart + (xGap * j);

            position.z = zStart + (zGap * rowNumber);

            instantiatedObject.transform.position = position;

            GridElement gridElement = instantiatedObject.GetComponent<GridElement>();
            gridElement.myRow = currentRowCount;
            gridElement.onCharacterLanded = onCharacterLandedOnGridElement;

            if (j == 0)
            {
                gridElement.myType = (ElementType)Random.Range(0, noOfTypes);
            }
            else
            {
                gridElement.myType = (ElementType)checkCorrespondence(currRowType[j - 1]);
            }

            GameObject particleSystemObject = GameObject.Instantiate(GetParticleSystemForElementType(gridElement.myType));
            gridElement.AddParticleSystem(particleSystemObject);

            currRowType.Add((int)gridElement.myType);
        }

        previousRowType.Clear();
        previousRowType.AddRange(currRowType);
        currentRowCount++;
    }

    void onCharacterLandedOnGridElement(int gridElementRow) {
        if (gridElementRow + startingNumberOfRows > currentRowCount - 1)
        {
            GenerateRow(currentRowCount);
        }
    }

    GameObject GetParticleSystemForElementType(ElementType elementType)
    {
        if (elementType == ElementType.Fire)
            return fireParticleSystemPrefab;
        else if (elementType == ElementType.Water)
            return waterParticleSystemPrefab;
        else if (elementType == ElementType.Wood)
            return woodParticleSystemPrefab;
        else if (elementType == ElementType.Thunder)
            return thunderParticleSystemPrefab;
        else if (elementType == ElementType.Rocks)
            return rocksParticleSystemPrefab;
        else if (elementType == ElementType.Sand)
            return sandParticleSystemPrefab;
        else if (elementType == ElementType.Wind)
            return windParticleSystemPrefab;
        else if (elementType == ElementType.Ice)
            return iceParticleSystemPrefab;
        //else if (elementType == ElementType.Mirror)
        //    return mirrorParticleSystemPrefab;
        else
            return fireParticleSystemPrefab;
    }

    int checkCorrespondence(int previousTileType)
    {
        int randomType = Random.Range(0, noOfTypes);
        while (UtilityFunctions.correspondence[randomType, previousTileType] == 0 || randomType == 5) //5 for mirror type, dont want mirrors for now
        {
            randomType = Random.Range(0, noOfTypes);
        }
        return randomType;
    }
}
