﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ElementType
{
    Water,
    Fire,
    Wood,
    Wind,
    Sand,
    Mirror,
    Thunder,
    Rocks,
    Ice,
    None
};
