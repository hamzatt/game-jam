﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    public ElementType myType;

    //Particle Systems
    public GameObject fireParticleSystem;
    public GameObject waterParticleSystem;
    public GameObject woodParticleSystem;
    public GameObject sandParticleSystem;
    public GameObject mirrorParticleSystem;
    public GameObject thunderParticleSystem;
    public GameObject iceParticleSystem;
    public GameObject windParticleSystem;
    public GameObject rocksParticleSystem;

    GameObject currentParticleSystem;

    // Use this for initialization
    void Start () {
        myType = ElementType.None;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnCollisionEnter(Collision collision)
    {
        GameObject gridElementObject = collision.gameObject;
        GridElement gridElement = gridElementObject.GetComponent<GridElement>();

        if (gridElement)
        {
            if (myType != ElementType.None)
            {
                if (UtilityFunctions.correspondence[(int)myType, (int)gridElement.myType] == 1)
                {
                    gridElement.RemoveParicleSystem();
                    SetType(gridElement.myType);
                }
                else
                {
                    Die();
                }
            }
            else
            {
                SetType(gridElement.myType);
                gridElement.RemoveParicleSystem();
            }
        }
    }

    void Die()
    {
        Debug.Log("Player Died");
    }

    void SetType(ElementType typeToSet)
    {
        myType = typeToSet;

        if (currentParticleSystem)
            currentParticleSystem.SetActive(false);

        currentParticleSystem = GetParticleSystemForElementType(myType);
        currentParticleSystem.SetActive(true);
    }

    GameObject GetParticleSystemForElementType(ElementType elementType)
    {
        if (elementType == ElementType.Fire)
            return fireParticleSystem;
        else if (elementType == ElementType.Water)
            return waterParticleSystem;
        else if (elementType == ElementType.Wood)
            return woodParticleSystem;
        else if (elementType == ElementType.Thunder)
            return thunderParticleSystem;
        else if (elementType == ElementType.Rocks)
            return rocksParticleSystem;
        else if (elementType == ElementType.Sand)
            return sandParticleSystem;
        else if (elementType == ElementType.Wind)
            return windParticleSystem;
        else if (elementType == ElementType.Ice)
            return iceParticleSystem;
        //else if (elementType == ElementType.Mirror)
        //    return mirrorParticleSystem;
        else
            return fireParticleSystem;
    }
}
