﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class GridElement : MonoBehaviour {

    public ElementType myType;
    public int myRow;
    public Action<int> onCharacterLanded;
    public GameObject particleSystemContainer;

    // Use this for initialization
    void Start () {
    
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnCollisionEnter(Collision collision) {
        onCharacterLanded(myRow);
    }

    void OnCollisionExit(Collision collision) {
        particleSystemContainer.gameObject.SetActive(true);
    }

    public void AddParticleSystem(GameObject particleSystemObject) {
        particleSystemObject.transform.SetParent(particleSystemContainer.transform,false);
    }

    public void RemoveParicleSystem() {
        particleSystemContainer.gameObject.SetActive(false);
    }
}
