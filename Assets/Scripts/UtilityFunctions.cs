﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class UtilityFunctions{

    public  static int[,] correspondence = {
        {1,1,0,0,0,0,0,1},
        {0,1,1,0,0,0,0,0},
        {0,0,1,1,1,0,1,0},
        {0,1,0,1,1,0,0,0},
        {0,1,1,0,1,0,1,1},
        {0,0,0,0,0,0,0,0},
        {1,0,0,1,0,0,1,0},
        {0,1,1,1,0,0,0,1},
        {0,0,0,1,1,0,0,0}};
}
